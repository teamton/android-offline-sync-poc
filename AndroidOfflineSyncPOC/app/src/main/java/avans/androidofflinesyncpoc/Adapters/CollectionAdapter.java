package avans.androidofflinesyncpoc.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import avans.androidofflinesyncpoc.Helpers.CacheHelper;
import avans.androidofflinesyncpoc.Helpers.RecordHelper;
import avans.androidofflinesyncpoc.Helpers.RequestManager;
import avans.androidofflinesyncpoc.R;

/**
 *
 * @author Toine Bakkeren
 *
 */
public class CollectionAdapter extends ArrayAdapter<String> implements Listener<JSONObject>, ErrorListener {

    private final String TAG = getClass().getSimpleName();

    private int currentPage = 1;

    private ImageLoader mImageLoader;

    /**
     *  The data that drives the adapter
     */
    public final List<JSONObject> mData = new ArrayList<JSONObject>();

    /**
     * The last network response containing twitter metadata
     */
    private JSONObject mCollectionData;

    private boolean isLoading;

    /**
     * Flag telling us our last network call returned 0 results and we do not need to execute any new requests
     */
    private boolean moreDataToLoad;

    public CollectionAdapter(Context context, JSONObject newData) {
        super(context, R.layout.record_item_layout);

        try {
            JSONArray releases = newData.getJSONArray("releases");
            proccessData(releases);
            }
        catch (Exception e) {
            e.printStackTrace();
        }

        mCollectionData = newData;

        moreDataToLoad = true;

        mImageLoader = new ImageLoader(RequestManager.getRequestQueue(), new ImageLoader.ImageCache() {
            private final LruCache<String, Bitmap> mCache = new LruCache<String, Bitmap>(10);
            public void putBitmap(String url, Bitmap bitmap) {
                mCache.put(url, bitmap);
            }
            public Bitmap getBitmap(String url) {
                return mCache.get(url);
            }
        });
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        try {

        View v = convertView;
        ViewHolder viewHolder;

        //check to see if we need to load more data
        if(shouldLoadMoreData(mData, position) ) {
            loadMoreData();
        }

        if(v == null){
            LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.record_item_layout, parent, false);

            viewHolder = new ViewHolder();

            viewHolder.recordThumbnail = (NetworkImageView) v.findViewById(R.id.record_thumb);
            viewHolder.recordTitle = (TextView) v.findViewById(R.id.record_title);
            viewHolder.recordArtist = (TextView) v.findViewById(R.id.record_artist);

            v.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) v.getTag();
        }

        JSONObject record = mData.get(position);
        if(record != null){
            JSONObject basicInfo = record.getJSONObject("basic_information");

            viewHolder.recordThumbnail.setImageUrl(basicInfo.getString("thumb"), mImageLoader);
            viewHolder.recordTitle.setText(basicInfo.getString("title") + " (" + basicInfo.getString("year") + ")");
            viewHolder.recordArtist.setText(basicInfo.getJSONArray("artists").getJSONObject(0).getString("name"));
        }

        return v; }
        catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public int getCount() {
        return mData.size();
    }


    private boolean shouldLoadMoreData(List<JSONObject> data, int position){
        // If showing the last set of data, request for the next set of data
        boolean scrollRangeReached = (position > (data.size() - 50));
        return (scrollRangeReached && !isLoading && moreDataToLoad);
    }

    private void loadMoreData(){
        isLoading = true;
        Log.v(getClass().toString(), "Load more records");
        RecordHelper.getInstance().getCollection(this, this, currentPage + 1);
        currentPage++;
    }


    /**
     * Viewholder for the listview row
     *
     *
     */
    static class ViewHolder{
        NetworkImageView recordThumbnail;
        TextView recordTitle;
        TextView recordArtist;
    }

    @Override
    public void onResponse(JSONObject response) {
        if(response != null){
            try {
                String URL = "https://api.discogs.com/users/soundman12/collection/folders/0/releases?page=" + currentPage;
                CacheHelper.put(URL, response.toString());
                JSONArray releases = response.getJSONArray("releases");

                proccessData(releases);

                mCollectionData = response;

                if(currentPage <= response.getJSONObject("pagination").getInt("pages")) {
                    moreDataToLoad = true;
                }
                else {
                    moreDataToLoad = false;
                }

                notifyDataSetChanged();
                Log.v(TAG, "New records retrieved");

            } catch(Exception e){
                e.printStackTrace();
            }
        }

        isLoading = false;
    }

    private void proccessData(JSONArray releases) {
        try {
            for (int i=0;i<releases.length();i++){
                mData.add(releases.getJSONObject(i));
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.e(TAG, "Error retrieving additional records: " + error.getMessage());
        String URL = "https://api.discogs.com/users/soundman12/collection/folders/0/releases?page=" + currentPage;
        String JSON = CacheHelper.get(URL);

        try {
            JSONObject response = new JSONObject(JSON);
            JSONArray releases = response.getJSONArray("releases");

            proccessData(releases);

            mCollectionData = response;

            if(currentPage <= response.getJSONObject("pagination").getInt("pages")) {
                moreDataToLoad = true;
            }
            else {
                moreDataToLoad = false;
            }

            notifyDataSetChanged();
            Log.v(TAG, "New records retrieved");

        } catch(Exception e){
            e.printStackTrace();
        }

        isLoading = false;
    }
}