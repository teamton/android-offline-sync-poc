package avans.androidofflinesyncpoc.Fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONObject;

import avans.androidofflinesyncpoc.Adapters.CollectionAdapter;
import avans.androidofflinesyncpoc.Helpers.CacheHelper;
import avans.androidofflinesyncpoc.Helpers.RecordHelper;
import avans.androidofflinesyncpoc.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ListFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ListFragment extends Fragment {
    private OnFragmentInteractionListener mListener;
    private ProgressDialog progressDialog;

    private int page = 1;
    private String URL = "https://api.discogs.com/users/soundman12/collection/folders/0/releases?page=" + page;

    private JSONObject mViewData;
    private ListView mListView;

    public ListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_list, container, false);

        mListView = (ListView) view.findViewById(R.id.record_list);

        if (mViewData == null) {
            // Get the first page
            RecordHelper.getInstance().getCollection(createMyReqSuccessListener(), createMyReqErrorListener(), 1);
        }
        else {
            mListView.setAdapter(new CollectionAdapter(getActivity(), mViewData));
            setOnClickListener();
        }

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mListener = (OnFragmentInteractionListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onListFragmentInteraction(JSONObject jsonObject);
    }

    private void setOnClickListener() {
        /*mListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                CollectionAdapter adapter = (CollectionAdapter) mListView.getAdapter();
                JSONObject selected = adapter.mData.get(position);

                try {
                    Log.d("SELECTED", selected.getJSONObject("basic_information").getString("title"));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                mListener.onListFragmentInteraction(selected);
            }
        });*/
    }

    private Response.Listener<JSONObject> createMyReqSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.v("FRAGMENT", "Data loaded");
                mViewData = response;
                page++;
                CacheHelper.put(URL, response.toString());
                mListView.setAdapter(new CollectionAdapter(getActivity(), mViewData));
                setOnClickListener();
            }
        };
    }

    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String JSON = CacheHelper.get(URL);
                Log.d("DATA", JSON);

                try {
                    JSONObject object = new JSONObject(JSON);
                    mViewData = object;
                    mListView.setAdapter(new CollectionAdapter(getActivity(), mViewData));
                    page++;
                } catch(Exception e) {
                    e.printStackTrace();
                }


                Log.e("FRAGMENT", "Data failed to load"
                );
            }
        };
    }

}
