package avans.androidofflinesyncpoc.Helpers;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by toinebakkeren on 31-03-16.
 */
public class RecordHelper {

    private final String TAG = getClass().getSimpleName();
    private static RecordHelper mInstance;

    public static RecordHelper getInstance(){
        if(mInstance == null) {
            mInstance = new RecordHelper();
        }

        return mInstance;
    }

    public String getCollection(Response.Listener<JSONObject> listener, Response.ErrorListener errorListener, int pageNum){

        String url = "https://api.discogs.com/users/soundman12/collection/folders/0/releases?page=" + pageNum;
        Log.i(TAG, "getCollection: uri = " + url);

        JsonObjectRequest request = new JsonObjectRequest
                (Request.Method.GET, url, null, listener, errorListener) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String AKey = "TnsAbGZogXbADybWDqSZ";
                String ASecret = "XEZUQbpPrEbbSPOebwYcJqcZTHBSFVAB";

                //, XEZUQbpPrEbbSPOebwYcJqcZTHBSFVAB

                HashMap<String, String> params = new HashMap<String, String>();

                params.put("User-Agent", "EindopdrachtMBD2/0.1 +http://tonnie.me");
                params.put("Authorization","Discogs key=" + AKey + ", secret=" + ASecret);

                return params;
            }
        };

        RequestManager.getRequestQueue().add(request);
        return url;
    }

    public void getSingleRecord(Response.Listener<JSONObject> listener, Response.ErrorListener errorListener, String releaseURL) {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, releaseURL,null,listener,errorListener) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String AKey = "TnsAbGZogXbADybWDqSZ";
                String ASecret = "XEZUQbpPrEbbSPOebwYcJqcZTHBSFVAB";

                HashMap<String, String> params = new HashMap<String, String>();

                params.put("User-Agent", "EindopdrachtMBD2/0.1 +http://tonnie.me");
                params.put("Authorization","Discogs key=" + AKey + ", secret=" + ASecret);

                return params;
            }
        };

        RequestManager.getRequestQueue().add(request);
    }
}
