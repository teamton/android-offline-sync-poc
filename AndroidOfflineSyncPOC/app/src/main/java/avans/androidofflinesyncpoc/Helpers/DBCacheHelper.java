package avans.androidofflinesyncpoc.Helpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by toinebakkeren on 09-04-16.
 */
public class DBCacheHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 2;
    private static final String TABLE_NAME = "datacache";
    private static final String TABLE_CREATE =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    "URL" + " TEXT, " +
                    "RESPONSE" + " TEXT);";

    DBCacheHelper(Context context) {
        super(context, "ANDROIDPOC", null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int param1, int param2) {

    }
}
