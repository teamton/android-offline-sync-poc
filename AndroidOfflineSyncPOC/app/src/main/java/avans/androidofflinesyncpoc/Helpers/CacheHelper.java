package avans.androidofflinesyncpoc.Helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.vincentbrison.openlibraries.android.dualcache.lib.DualCache;
import com.vincentbrison.openlibraries.android.dualcache.lib.DualCacheBuilder;
import com.vincentbrison.openlibraries.android.dualcache.lib.DualCacheContextUtils;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.net.URLEncoder;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by toinebakkeren on 09-04-16.
 */
public class CacheHelper {

    static DBCacheHelper dbCache;

    public static void put(String key, String value) {

        try {
            SQLiteDatabase db = dbCache.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put("URL", key);
            values.put("RESPONSE", value);

            // Insert the new row, returning the primary key value of the new row
            long newRowId;
            newRowId = db.insert(
                    "datacache",
                    "RESPONSE",
                    values);
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    public static void init(Context context) {
        dbCache = new DBCacheHelper(context);
    }

    public static String get(String key) {

        try {
            SQLiteDatabase db = dbCache.getReadableDatabase();
            Cursor c = db.query(false, "datacache", new String[] {"RESPONSE"}, "URL = ?", new String[]{key}, null, null, null, null);

            c.moveToFirst();
            return c.getString(0);

        } catch (Exception e) {

        }

        return "";
    }

}
